# -*- coding: utf-8 -*-
from setuptools import setup

package_dir = \
{'': 'src'}

packages = \
['loadingpy']

package_data = \
{'': ['*']}

setup_kwargs = {
    'name': 'loadingpy',
    'version': '0.1.4',
    'description': '',
    'long_description': '# loadingpy\n\nIn this repository, we provide a custom made progress bar for python iterables. This library can be used as is or modified for any purposes (see licence).\n\n## for deep learning\n\nThere is now a new progress bar available for deep learning purposes (I guess it could be leveraged for other stuff as well...). Say, you want to train a model using a dataset $D$ over $e$ epochs. Using `TrainBar`you can get a double progress bar (first for the epochs and second for the steps in the current epoch) on a single line. you can check the [test](tests/test_loadingpy.py) or this simple example:\n\n```python\nfrom loadingpy import TrainBar\n\nfor data in TrainBar(\n        trainloader,\n        num_epochs=e,\n        base_str="training",\n    ):\n        inputs, labels = data\n```\n\n## Example\nYou can install with pip `pip install loadingpy` and use as follows\n\n```python\nfrom loadingpy import PyBar\n\nloss = 0.0\naccuracy = 0.0\nfor inputs, labels in PyBar(dataset, monitoring=[loss, accuracy], naming=["loss", "accuracy"], base_str="training"):\n    # do whatever you please\n    loss += 0.0 # update monitoring variables in place\n    accuracy += 0.0 # update monitoring variables in place\n```\n\nFor a more detailed exampel (in torch) check this [tutorial](https://gitlab.com/ey_datakalab/loadingpy/-/blob/main/notebooks/unit_test.ipynb). You can use a global argument in order to disable the verbatim from the loading bars as follows:\n\n```python\nfrom loadingpy import BarConfig\n\nBarConfig["disable loading bar"] = True\n```\n\n## Arguments\n\nHere is a list of the arguments and their description\n| argument | description | type |\n| :---: | :---: | :---: |\n| iterable | python object that can be iterated over | can be a list, tuple, range, np.ndarray, torch.Tensor, dataset,... |\n| monitoring | a python object (or list of python objects) that will be printed after each iteration using the following format f\'{monitoring}\'. IF they are updated during the loop, make sure to update inplace, in order to see the changes | an be a tensor, float or list of these |\n| naming | if you want to add a descritpion prefix to the monitoring variables | str or list of str |\n| total_steps | number of iterations to perform (if you set it to a lower value than the length of the iterable, then the process will stop after the given total_steps) | int |\n| base_str | prefix description of the loop we are iterating over | str |\n| color | which color to use for the loading bar | str |',
    'author': 'Edouard Yvinec',
    'author_email': 'edouardyvinec@hotmail.fr',
    'maintainer': 'None',
    'maintainer_email': 'None',
    'url': 'None',
    'package_dir': package_dir,
    'packages': packages,
    'package_data': package_data,
    'python_requires': '>=3.8,<4.0',
}


setup(**setup_kwargs)

